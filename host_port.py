class HostAndPort(object):

    def __init__(self, host, port=-1):
        super(HostAndPort, self).__init__()
        self.__host = host
        self.__port = port

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return int(self.__port)

    def is_localhost(self):
        return self.host.startswith("127.")

    def empty(self):
        return not self.host and self.port < 0

    @classmethod
    def is_valid_host_port(cls, text):
        try:
            cls.from_text(text)
            return True
        except ValueError:
            return False

    @classmethod
    def from_text(cls, text):
        parts = text.split(":")
        if len(parts) < 2:
            raise ValueError("Invalid format: {} try host:port!".format(text))
        return cls(parts[0], int(parts[1]))

    def to_string(self):
        return self.__str__()

    def __str__(self):
        return "{}:{}".format(self.host, self.port)
