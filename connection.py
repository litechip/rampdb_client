from .socketclient.client import Client
from .client_protocol import ClientProtocol
from .database import Database
from .connection_string import ConnectionString


class Connection(object):
    def __init__(self, host, port, timeout=1):
        self._socket = Client(host, port, timeout)
        self._protocol = ClientProtocol(self._socket)

    def get_database_names(self):
        reply, err = self.send_command("database_names", {})
        if err:
            return err
        return self.protocol.ok_status(reply)

    def add_slave_to_cluster(self, hostname):
        c = 's.add_slave'
        cmd = {
            'p': {
                'hostname': hostname
            }
        }

        return self.protocol.send(c, cmd)

    def set_as_master(self, master_host):
        c = 's.set_as_master'
        cmd = {
            'p': {
                'host': master_host,
            }
        }
        return self.protocol.send(c, cmd)

    def cluster_status(self):
        c = 's.cluster_status'
        cmd = {'p': {}}
        return self.protocol.send(c, cmd)

    def connect(self):
        pass

    def select_database(self, name):
        return Database(self, name)

    def select_collection(self, db, coll):
        d = self.select_database(db)
        return d.select_collection(coll)

    def set_protocol(self, v):
        self._protocol = v

    @property
    def protocol(self):
        return self._protocol

    def send_command(self, name, params):
        c = "c." + name

        cmd = {
            "p": params
        }

        return self.protocol.send(c, cmd)

    def get_entity_from_ns(self, ns):
        parts = ns.split(".")
        ln = len(parts)
        if ln > 2:
            raise ValueError("Bad ns resolution for: {}".format(ns))
        if ln == 1:
            return self.select_database(parts[0])

        if ln == 2:
            return self.select_collection(parts[0], parts[1])

        return self

    def exec_command(self, name, ns, params):
        entity = self.get_entity_from_ns(ns)
        if hasattr(entity, name) and callable(getattr(entity, name)):
            result = getattr(entity, name)(**params)
            return result
        raise ValueError("Cannot execute command {} {}".format(name, ns))

    @classmethod
    def from_conn_string(cls, conn: ConnectionString):
        # TODO REFACTOR FOR REPLICA SET
        host_port = conn.get_master_host_port()
        return cls(host_port.host, host_port.port)
