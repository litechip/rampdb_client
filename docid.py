import binascii
import calendar
import datetime
import hashlib
import socket
import struct
import time
import os

from bson.errors import InvalidId
from bson.tz_util import utc


def _hostname_bytes():
    m = hashlib.md5()
    m.update(socket.gethostname().encode())
    return m.digest()[0:3]


class DocId(object):

    _hostname_bytes = _hostname_bytes()

    def __init__(self, oid=None):
        """Initialize a new DocId.


        An DocId is a 12-byte unique identifier consisting of:

          - a 4-byte value representing the seconds since the Unix epoch,
          - a 3-byte machine identifier,
          - a 5-byte random

          """

        if oid is None:
            self.__create()
        elif isinstance(oid, bytes) and len(oid) == 12:
            self.__id = oid
        else:
            self.__validate(oid)

    @classmethod
    def from_datetime(cls, from_time):
        if from_time.utcoffset() is not None:
            from_time = from_time - from_time.utcoffset()
        timestamp = calendar.timegm(from_time.timetuple())
        oid = struct.pack(
            ">i", int(timestamp)) + b"\x00\x00\x00\x00\x00\x00\x00\x00"
        return cls(oid)

    @classmethod
    def is_valid(cls, oid):
        try:
            if not oid:
                return False

            DocId(oid)
            return True
        except (InvalidId, TypeError):
            return False

    def __create(self):
        """Generate a new value for this DocId.
        """

        # 4 bytes current time
        oid = struct.pack(">i", int(time.time()))

        # 3 bytes machine
        oid += DocId._machine_bytes

        # 5 bytes random
        oid += os.urandom(5)

        self.__id = oid

    def __validate(self, oid):
        ex = InvalidId(
            "%r is not a valid DocId, it must be a 12-byte input" % oid)
        if isinstance(oid, DocId):
            self.__id = oid.to_binary()
        elif isinstance(oid, str):
            if len(oid) == 12:
                try:
                    self.__id = str.encode(oid)
                except (TypeError, ValueError):
                    raise ex
            else:
                raise ex
        else:
            raise TypeError("id must be an instance of (bytes,DocId)")

    @property
    def to_binary(self):
        return self.__id

    @property
    def generation_time(self):
        timestamp = struct.unpack(">i", self.__id[0:4])[0]
        return datetime.datetime.fromtimestamp(timestamp, utc)

    def __str__(self):
        return binascii.hexlify(self.__id).decode()

    def __repr__(self):
        return "DocId('%s')" % (str(self),)

    def __eq__(self, other):
        if isinstance(other, DocId):
            return self.__id == other.to_binary()
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, DocId):
            return self.__id != other.to_binary()
        return NotImplemented

    def __lt__(self, other):
        if isinstance(other, DocId):
            return self.__id < other.to_binary()
        return NotImplemented

    def __le__(self, other):
        if isinstance(other, DocId):
            return self.__id <= other.to_binary()
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, DocId):
            return self.__id > other.to_binary()
        return NotImplemented

    def __ge__(self, other):
        if isinstance(other, DocId):
            return self.__id >= other.to_binary()
        return NotImplemented

    def __hash__(self):
        return hash(self.__id)
