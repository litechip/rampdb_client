from .collection import Collection


class Database(object):

    def __init__(self, connection, database_name):
        self.__connection = connection
        self._database_name = database_name

    def select_collection(self, coll_name):
        return Collection(self, coll_name)

    def create_collection(self, name, options=None):

        if not isinstance(name, str):
            raise TypeError("collection name  must be an instance of str")

        # TODO add validation
        p = {
            "name": name,
            "options": options
        }

        reply, err = self.send_command("create_collection", p)
        return self.send_response(reply, err)

    def drop_collection(self, name):
        if not isinstance(name, str):
            raise TypeError("collection name  must be an instance of str")

        p = {
            "name": name,
        }

        reply, err = self.send_command("drop_collection", p)
        return self.send_response(reply, err)

    def run_command(self, name, **params):
        if not isinstance(name, str):
            raise TypeError("command name  must be an instance of str")

        p = {
            "name": name,
            "options": params
        }

        reply, err = self.send_command("command", p)
        return self.send_response(reply, err)

    def collection_names(self):
        reply, err = self.send_command("collection_names", {})
        return self.send_response(reply, err)

    @property
    def connection(self):
        return self.__connection

    @property
    def name(self):
        return self._database_name

    def __str__(self):
        return self._database_name

    def __getitem__(self, collection_name):
        return Collection(self, collection_name)

    def __repr__(self):
        return "Database({}, {})".format(self.protocol, self._database_name)

    @property
    def protocol(self):
        return self.__connection.protocol

    def send_response(self, reply, err, *args):
        if err:
            return err
        data = {}
        for arg in args:
            # TODO add validation
            arg_val = reply.get(arg, None)
            data[arg] = arg_val

        if not data:
            data = reply

        return self.protocol.ok_status(**data)

    def send_command(self, name, params):
        c = "d." + name

        additional_p = {
            "ns": self._database_name
        }

        params.update(additional_p)

        cmd = {
            "p": params
        }

        reply, err = self.protocol.send(c, cmd)
        return reply, err
