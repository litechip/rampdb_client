from .host_port import HostAndPort


class ConnectionType(object):

    def __init__(self, type):
        self.__type = type

    def is_master(self):
        return self.__type == "master"

    def is_invalid(self):
        return self.__type == "invalid"

    def is_set(self):
        return self.__type == "set"

    def is_sync(self):
        return self.__type == "sync"

    def is_custom(self):
        return self.__type == "custom"

    @classmethod
    def master(cls):
        return cls("master")

    @classmethod
    def invalid(cls):
        return cls("invalid")

    @classmethod
    def set(cls):
        return cls("set")

    @classmethod
    def sync(cls):
        return cls("sync")

    @classmethod
    def custom(cls):
        return cls("custom")

    def __str__(self):
        return str(self.__type)


class ConnectionString(object):

    def __init__(self):
        self.__type = ConnectionType.invalid()
        self.__servers = []
        self.__replica_name = False

    def add_server(self, host_port: HostAndPort):
        self.__servers.append(host_port)

    def get_master_host_port(self):
        if self.type.is_master():
            return self.__servers[0]
        raise TypeError(
            "Connection string is not master but: {}".format(self.type))

    @property
    def host_and_ports(self):
        return self.__servers

    @property
    def replica_name(self):
        return self.__replica_name

    @replica_name.setter
    def replica_name(self, name):
        self.__replica_name = name

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, conn: ConnectionType):
        self.__type = conn

    def fill_servers(self, conn_str):
        s = conn_str.split(",")
        for conn in s:
            self.add_server(HostAndPort.from_text(conn))

    def to_string(self):
        conn_strs = []
        for host_port in self.__servers:
            conn_strs.append(host_port.to_string())

        members = ','.join(conn_strs)

        if self.type.is_set():
            template = "{}/{}"
            return template.format(
                self.replica_name,
                members
            )

        return members

    @classmethod
    def is_valid(cls, string):
        return HostAndPort.is_valid_host_port(string)

    @classmethod
    def from_host_and_port(cls, host_port: HostAndPort):
        conn = cls()
        conn.type = ConnectionType.master()
        conn.add_server(host_port)
        return conn

    @classmethod
    def from_text(cls, text):
        if "/" in text:
            # replica set case
            replica, hosts = text.split("/")
            return cls.from_conn_type(ConnectionType.set(), hosts, replica)
        else:
            return cls.from_conn_type(ConnectionType.master(), text)

    @classmethod
    def from_conn_type(cls, conn_type: ConnectionType,  s, replica_name=""):
        conn = cls()

        # TODO add nr servers validation dbclientinterface
        conn.type = conn_type
        conn.fill_servers(s)
        conn.replica_name = replica_name
        return conn

    @classmethod
    def from_conn_strings(cls, hosts):
        conn_strings = []
        for cfg_host in hosts:
            host_port = HostAndPort.from_text(cfg_host)
            conn_string = ConnectionString.from_host_and_port(host_port)
            conn_strings.append(
                conn_string
            )

        return conn_strings
