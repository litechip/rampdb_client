from bson.son import SON
from . import errors
from bson import ObjectId
from collections import OrderedDict


class Collection(object):
    def __init__(self, database, name):
        if not isinstance(name, str):
            raise TypeError("name must be a string!")

        if not name:
            raise errors.InvalidName("collection names cannot be empty")

        if name[0] == "." or name[-1] == ".":
            raise errors.InvalidName("collection names must not start "
                                     "or end with '.': %r" % name)
        if "\x00" in name:
            raise errors.InvalidName("collection names must not contain the "
                                     "null character")

        self._collection_name = name
        self._database = database

    def __str__(self):
        return "%s.%s" % (str(self._database), self._collection_name)

    def __repr__(self):
        return "Collection(%s, %s)" % (self._database, self._collection_name)

    def count(self, spec=None, fields=None):
        if fields is not None:
            if not fields:
                fields = ["_id"]
            fields = self._list_to_dummy_dict(fields)

        p = {
            "query": spec or SON(),
            "fields": fields
        }

        reply, err = self.send_command("count", p)

        if not err:
            return reply["count"]
        return err

    def by_example(self, spec=None, skip=0, limit=0, fields=None, filter=None, cursor=False,
                   **kwargs):
        if spec is None:
            spec = SON()

        if fields is not None and not isinstance(fields, (dict, list)):
            raise TypeError("fields must be an instance of dict or list")
        if not isinstance(spec, dict):
            raise TypeError("spec must be an instance of dict")
        if not isinstance(limit, int):
            raise TypeError("limit must be an instance of int")
        if not isinstance(skip, int):
            raise TypeError("skip must be an instance of int")

        if fields is not None:
            if not isinstance(fields, dict):
                if not fields:
                    fields = ["_id"]
                fields = self._list_to_dummy_dict(fields)

        params = {
            "n_to_skip": skip,
            "n_to_return": limit,
            "query": spec,
            "fields": fields
        }

        reply, err = self.send_command("query", params)

        if not err:
            return reply["data"]
        return err

    def one_example(self, spec=None, fields=None, **kwargs):
        return self.by_example(spec=spec, limit=1, fields=fields, **kwargs)

    def insert(self, docs: dict or list, **kwargs):
        ids = []

        def ensure_id(doc, app_ids):
            if doc.get('_id', None) is None:
                doc["_id"] = ObjectId()
            app_ids.append(doc.get('_id'))

        if isinstance(docs, dict):
            ensure_id(docs, ids)
            docs = [docs]

        elif isinstance(docs, list):
            ids = []
            for doc in docs:
                if isinstance(doc, dict):
                    ensure_id(doc, ids)

        flags = kwargs.get('flags', 0)
        params = {
            "ns": self.get_ns(),
            "flags": flags,
            "docs": docs
        }

        reply, err = self.send_command("insert", params)
        if not err:
            return self.ok_status(ids=ids)
        return err

    def update(self, spec, document, upsert=False, multi=False, **kwargs):
        if not isinstance(spec, dict):
            raise TypeError("spec must be a dict")
        if not isinstance(document, dict):
            raise TypeError("document must be a dict")
        if not isinstance(upsert, bool):
            raise TypeError("upsert must be a bool")

        p = {
            "query": spec,
            "update": document,
            "upsert": upsert,
            "multi": multi
        }

        reply, err = self.send_command("update", p)

        if not err:
            return self.ok_status(**reply["data"])
        return err

    def remove(self, spec, single=False, **kwargs):
        if isinstance(spec, ObjectId):
            spec = SON(dict(_id=spec))
        if not isinstance(spec, dict):
            raise TypeError(
                "spec must be an instance of dict, not %s" % type(spec))

        p = {
            "spec": spec,
            "single": single
        }

        reply, err = self.send_command("remove", p)

        if not err:
            return self.ok_status(**reply["data"])
        return err

    def drop(self, **kwargs):
        return self._database.drop_collection(self._collection_name)

    def create_index(self, name, keys, **kwargs):

        if not isinstance(name, str):
            raise TypeError("index name  must be an instance of str")

        if not isinstance(keys, OrderedDict):
            raise TypeError("index keys must be an instance of OrderedDict")

        p = {
            "name": name,
            "keys": keys,
            "options": kwargs
        }

        reply, err = self.send_command("create_index", p)
        if not err:
            return self.ok_status(**reply)
        return err

    def drop_index(self, index_identifier):
        if not isinstance(index_identifier, str):
            raise TypeError("index identifier  must be an instance of str")

        p = {
            "identifier": index_identifier
        }

        reply, err = self.send_command("drop_index", p)

        if err:
            return err
        return self.ok_status(**reply)

    def drop_indexes(self):
        return self.drop_index("*")

    def rename(self, new_name):
        if not isinstance(new_name, str):
            raise TypeError("collection name  must be an instance of str")

        p = {
            "new": new_name
        }

        reply, err = self.send_command("rename_collection", p)

        if err:
            return err

        return self.ok_status(**reply)

    def send_command(self, name, params):
        c = "c." + name

        additional_p = {
            "ns": self.get_ns()
        }

        params.update(additional_p)

        cmd = {
            "p": params
        }

        reply, err = self._database.protocol.send(c, cmd)
        return reply, err

    def get_ns(self):
        return "{}.{}".format(self._database.name, self._collection_name)

    def _list_to_dummy_dict(self, fields):
        d = {}
        for field in fields:
            if not isinstance(field, str):
                raise TypeError("fields must be a list!")
            d[field] = 1
        return d

    def ok_status(self, **kwargs):
        return self._database.protocol.ok_status(**kwargs)

    def err_status(self, err, **kwargs):
        return self._database.protocol.err_status(err, **kwargs)

    def send_response(self, reply, err, *args):
        return self._database.send_response(reply, err, *args)
