class ExitError(SystemExit):
    code = 1
    __message = None

    def __init__(self, message=None):
        self.__message = message
        super().__init__(message)

    def __str__(self):
        return '{}, code: {}, message: {}'.format(self.__class__.__name__, self.code, self.__message)

    def __format__(self, format_spec):
        return self.__str__()

    def __unicode__(self):
        return self.__str__()


class ConfigError(ExitError):
    code = 2


class InitError(ExitError):
    code = 3


class Error(Exception):
    code = 1
    __message = None

    def __init__(self, message=None):
        self.__message = message
        super().__init__(message)

    def __str__(self):
        return '{}, code: {}, {}'.format(self.__class__.__name__, self.code, self.__message)

    def __format__(self, format_spec):
        return self.__str__()

    def __unicode__(self):
        return self.__str__()


class SocketError(Error):
    code = 6


class SocketRecvError(Error):
    code = 4


class SocketTimeoutError(Error):
    code = 5


class DbError(Error):
    code = 10


class InvalidCommandError(DbError):
    code = 11


class InvalidName(DbError):
    code = 12
