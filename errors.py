# Collection errors
class PyDBError(Exception):
    """Base class for PyDBError all exceptions."""

class InvalidName(PyDBError):
    """Raised when an invalid name is used."""
