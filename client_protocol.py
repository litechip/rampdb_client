import bson


class ClientProtocol(object):

    def __init__(self, transport):
        self.transport = transport

    def send(self, name, params):
        params["c"] = name
        data = self.transport.send(bson.BSON.encode(params))
        response = bson.BSON.decode(data)
        err = self.validate_response(response)
        return response, err

    def validate_response(self, response):
        try:
            err_db = "Bad server response!"
            empty = err_db + "Got: {}".format(response)
            if not response:
                raise Exception(empty)

            status = response.get('ok', 0)
            if status == 0:
                msg = response.get('error', None)
                if isinstance(msg, bytes):
                    msg = msg.decode('utf-8')
                if msg is None:
                    msg = "Unknown error!"

                err = "DB error! " + msg
                raise Exception(err)

            return False
        except Exception as e:
            return self.err_status(str(e))

    def ok_status(self, **kwargs):
        d = {
            "ok": 1
        }

        d.update(kwargs)
        return d

    def err_status(self, err, **kwargs):
        d = {
            "ok": 0,
            "error": err
        }

        d.update(kwargs)
        return d
